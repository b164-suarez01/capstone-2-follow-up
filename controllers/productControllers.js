const Product = require("../models/Product");

//Get ALL products
module.exports.getAllProducts = () => {
	return Product.find({}).then( result => {
		return result;
	})
}
//Get all ACTIVE products
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then( result => {
		return result;
	})
}

//Get product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then( result => {
		return result;
	})
}


//Add a product by admin
module.exports.addProduct = (reqBody) =>{

console.log(reqBody);
                                 
	//Create a new object
	let newProduct = new Product ({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price
	});

	//Saves the created object
	return newProduct.save().then((product,error) => {
		//Product Creation failed
		if(error) {
			return false;
		}else{
			//if product creation is successful
			return true;
		}
	})
} 

//update Product's Name, Description OR price using patch method.
module.exports.updateProduct = (productId,reqBody) => {
	//Specify muna ung mga properties to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
	};

	//Find by Id and Update(id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId,updatedProduct).then((product,error) => {
		//product not updated
		if(error){
			return false;
		}else{
			//product updated successfully
			return true;
		}
	})
}

//archive a product
module.exports.archiveProduct = (productId,reqBody) => {
	//Specify muna ung mga properties to be updated
	let archiveProduct = {
		isActive:false
	};

	//Find by Id and Update(id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId,archiveProduct).then((product,error) => {
		//product not updated
		if(error){
			return false;
		}else{
			//product updated successfully
			return true;
		}
	})
}