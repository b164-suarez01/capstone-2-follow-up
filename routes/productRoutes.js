const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/productControllers");
const auth = require("../auth");


//Get ALL products
router.get("/all", auth.verify, (req, res) =>{
	ProductController.getAllProducts().then(result => res.send(result));
});



//Get all ACTIVE products
router.get("/active", auth.verify, (req, res) => {
	ProductController.getAllActive().then(result => res.send(result))
});

//Get single products
router.get("/:productId", auth.verify, (req, res) => {
	ProductController.getProduct(req.params.productId).then(result => res.send(result))
});


//Add a product
router.post("/add", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
		ProductController.addProduct(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})

//Update a product's price and isActive status
router.patch("/:productId/update", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})

//Archive a Product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.archiveProduct(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})




module.exports = router;