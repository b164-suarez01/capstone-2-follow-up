const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config();
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const port = 5000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//http://localhost:5000/api/users
app.use("/api/users",userRoutes);

app.use("/api/products", productRoutes);

mongoose.connect(process.env.DB_CONNECTION, {
	useNewUrlParser: true,
	useUnifiedTopology:true
})
mongoose.connection.once('open', () => console.log(`Now connected to MongoDB Atlas`));
app.listen(process.env.PORT, () => {
	console.log(`API is now online on port ${process.env.PORT}`)
})