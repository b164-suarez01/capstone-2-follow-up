const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({

	firstName: {
		type:String,
		required: [true,"First name is required"]
	},
	lastName: {
		type:String,
		required: [true,"Last name is required"]
	},
	mobileNo: {
		type: String,
		required: [true,"Mobile no. is required"]
	},
	email: {
		type: String,
		require: [true,"Email is required"]
	},
	password: {
		type: String,
		required: [true,"Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	addToCartOrders: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			}, 
			addedToCartOn: {
				type: Date,
				default : new Date()
			}

		}],
		
	checkOutOrders: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			}, 
			quantity: {
				type: String,
				default: 1
			},			
			paymentType: {
				type: String,
				//Options are Credit Card, Mobile Payment or COD
				default: ""
			},
			checkOutDate:{
				type:Date,
				default: new Date()
			}
		}]
})

module.exports = mongoose.model("User", userSchema);