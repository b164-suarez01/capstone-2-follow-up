const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course is required."]
	},
	description: {
		type: String,
		require: [true,"Description is required."]
	},
	price: {
		type: Number,
		required: [true,"Price is required."]
	},
	prouductAddedOn : {
		type: Date,
		default: new Date()
	},
	isActive: {
		type: Boolean,
		default: true
	},
	addToCartBy:[
		{
			userId:{
				type:String,
				default:""
			},
			addToCartDate:{
				type:Date,
				default: new Date()
			}
		}],
	checkOutBy:[
		{
			userId:{
				type:String
			},
			checkOutDate:{
				type:Date,
				default: new Date()
			},
			quantity:{
				type: Number
			}
		}]
	})

module.exports = mongoose.model("Product", productSchema);
